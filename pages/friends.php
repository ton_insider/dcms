<?php
/*/pages/friends.php — Друзья 5.1 */
include_once '../sys/inc/start.php';
$doc = new document(1); /* access */
$doc->title = __('Друзья'); // title
$ank = empty($_GET['id']) ? $user : new user($_GET['id']); // Анкета
$return = empty($_GET['return']) ? '/' : $_GET['return']; // Возврат

if(!$ank->id) { /* Если пользователь не найден в базе */
     header('Refresh: 1; url=' . $return); // куда шлём
    $doc->err(__('Нет данных'));
    exit; /* ошибка + выход */ } else

$my = ($user->id == $ank->id) ? true : false; /* Дабы каждый раз не проверять */
$doc->title = $my ? __('Мои друзья') : __('Друзья %s', $ank->login); # Заголовок

/* делаем пересчет своих новых друзей каждый раз заходя в список чьих-либо друзей */
$new = $db->prepare("SELECT COUNT(*) FROM `friends` WHERE `id_user` = ? AND `confirm` = '0'");
$new->execute(Array($ank->id));
$user->friend_new_count = $new->fetchColumn();

/* масив стран, дабы не получать их каждый раз при повторах */
static $countries = array();

if(!$ank->is_friend($user) && !$ank->vis_friends) {
    header('Refresh: 1; url=' . $return); // куда шлём
    $doc->err(__('Доступ к данной странице ограничен'));
    exit;
}

$posts = array();

$pages = new pages;

$res = $db->prepare("SELECT COUNT(*) FROM `friends` WHERE `id_user` = ? AND `confirm` = '1'");
$res->execute(Array($ank->id));

$pages->posts = $res->fetchColumn();

$q=$db->prepare("SELECT * FROM `friends` WHERE `id_user` = ? AND `confirm` = '1' ORDER BY `time` DESC LIMIT " . $pages->limit);
$q->execute(Array($ank->id));


$listing = new listing();
while ($arr = $q->fetchAll()) {
    foreach ($arr AS $friend) {
    $fr = new user($friend['id_friend']);
    $post = $listing->post();
    $post->icon($fr->icon());
    $post->title = $fr->nick();
#   $post->content[] = "ID: $fr->id"; 
    $post->url = '/profile.view.php?id=' . $fr->id;
if ($fr->ank_d_r && $fr->ank_m_r) { /* если известны день и месяц рождения */
if ($fr->ank_g_r) /* если известен год рождения, то показываем возраст */
    $post->content[] = __('Возраст') . ": " . misc::get_age($fr->ank_g_r, $fr->ank_m_r, $fr->ank_d_r, true);
    $post->content[] = $fr->ank_g_r ? __('Дата рождения') . ": $fr->ank_d_r.$fr->ank_m_r.$fr->ank_g_r" : __('День рождения') . ": $fr->ank_d_r " . misc::getLocaleMonth($fr->ank_m_r);
} /* конец блока с датой рождения и возрастом "друга" */
if ($fr->realname) /* очень хитровыебаный блок, который отображает: 
    ФИО, если заполнено фамилия, имья и отчество, 
    Вася Пупкин или Василий Петрович, если два из 3-х, 
    Или-же просто имья, если фамилия и отчество пусты. */
    $post->content[] = ($fr->lastname && $fr->middle_n) ? __('ФИО') . ": $fr->lastname $fr->realname $fr->middle_n" : __('Имя') . ": $fr->realname" . ($fr->middle_n ? " $fr->middle_n":'') . ($fr->lastname ? " $fr->lastname":'');
if ($fr->country) {
if(!isset($countries[$fr->country])) { // если нет в кеше, то получаем и кешируем

    $rs = $db->prepare("SELECT * FROM `countries` WHERE `code` = ?");
    $rs->execute(Array($fr->country));
    $rs = $rs->fetch();

    $countries[$fr->country] = ($user->country == $fr->country)? $rs['country_n'] : $rs['english_n'];
    } // конец кеширования названия страны; Вывод страны и региона, если он выбран
    $post->content[] = ($fr->region_c && $fr->region_t)? __('Регион') . ": $fr->region_t, " . $countries[$fr->country] : __('Страна') . ': ' . $countries[$fr->country];
} // конец блока со страной и регионом "друга" */
    $post->content[] = __('Дата регистрации') . ': ' . date('d.m.Y', $fr->reg_date); 
if ($fr->last_visit && $user->id)
    $post->content[] = __('Последний визит') . ': ' . misc::when($fr->last_visit); 
    $post->content[] = $friend['confirm'] ? '' : '[b]' . __('Хочет быть Вашим другом') . '[/b]'; 
    }
}

$listing->display($my ? __('Друзей нет') : __('У пользователя "%s" еще нет друзей', $ank->login));

$pages->display('?id=' . $ank->id . '&amp;'); // вывод страниц

$doc->ret(__('Анкета «%s»', $ank->login), '/profile.view.php?id=' . $ank->id);
#«Друзья 5.1» by @S1S13AF7. // пример: 2k1x.TK/friends.php (необходимо иметь профиль)