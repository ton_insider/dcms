/* Данные таблицы `countries` */
INSERT INTO `countries` (`code`, `english_n`, `country_n`) VALUES 
('AF', 'Afghanistan', 'افغانستان'), 
('AX', 'Aland Islands', 'Aland Islands'), 
('AL', 'Albania', 'Shqipëria'), 
('DZ', 'Algeria', 'الجزائر'), 
('AS', 'American Samoa', 'American Samoa'), 
('AD', 'Andorra', 'Andorra'), 
('AO', 'Angola', 'Angola'), 
('AI', 'Anguilla', 'Anguilla'), 
('AQ', 'Antarctica', 'Antarctica'), 
('AG', 'Antigua and Barbuda', 'Antigua and Barbuda'), 
('AR', 'Argentina', 'Argentina'), 
('AM', 'Armenia', 'Հայաստան'), 
('AW', 'Aruba', 'Aruba'), 
('AU', 'Australia', 'Australia'), 
('AT', 'Austria', 'Österreich'), 
('AZ', 'Azerbaijan', 'Azərbaycan'), 
('BS', 'Bahamas', 'Bahamas'), 
('BH', 'Bahrain', 'البحرين'), 
('BD', 'Bangladesh', 'বাংলাদেশ'), 
('BB', 'Barbados', 'Barbados'), 
('BY', 'Belarus', 'Белару́сь'), 
('BE', 'Belgium', 'België'), 
('BZ', 'Belize', 'Belize'), 
('BJ', 'Benin', 'Bénin'), 
('BM', 'Bermuda', 'Bermuda'), 
('BT', 'Bhutan', 'Bhutan'), 
('BO', 'Bolivia', 'Bolivia'), 
('BA', 'Bosnia and Herzegovina', 'Bosna i Hercegovina'), 
('BW', 'Botswana', 'Botswana'), 
('BV', 'Bouvet Island', 'Bouvet Island'), 
('BR', 'Brazil', 'Brasil'), 
('IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory'), 
('BN', 'Brunei', 'Brunei Darussalam'), 
('BG', 'Bulgaria', 'България'), 
('BF', 'Burkina Faso', 'Burkina Faso'), 
('BI', 'Burundi', 'Uburundi'), 
('KH', 'Cambodia', 'Kampuchea'), 
('CM', 'Cameroon', 'Cameroun'), 
('CA', 'Canada', 'Canada'), 
('CV', 'Cape Verde', 'Cabo Verde'), 
('KY', 'Cayman Islands', 'Cayman Islands'), 
('CF', 'Central African Republic', 'République Centrafricaine'), 
('TD', 'Chad', 'Tchad'), 
('CL', 'Chile', 'Chile'), 
('CN', 'China', '中国'), 
('CX', 'Christmas Island', 'Christmas Island'), 
('CC', 'Cocos Islands', 'Cocos Islands'), 
('CO', 'Colombia', 'Colombia'), 
('KM', 'Comoros', 'Comores'), 
('CG', 'Congo', 'Congo'), 
('CD', 'Congo, Democratic Republic of the', 'Congo, Democratic Republic of the'), 
('CK', 'Cook Islands', 'Cook Islands'), 
('CR', 'Costa Rica', 'Costa Rica'), 
('CI', 'Côte d&#39;Ivoire', 'Côte d&#39;Ivoire'), 
('HR', 'Croatia', 'Hrvatska'), 
('CU', 'Cuba', 'Cuba'), 
('CY', 'Cyprus', 'Κυπρος'), 
('CZ', 'Czech Republic', 'Česko'), 
('DK', 'Denmark', 'Danmark'), 
('DJ', 'Djibouti', 'Djibouti'), 
('DM', 'Dominica', 'Dominica'), 
('DO', 'Dominican Republic', 'Dominican Republic'), 
('EC', 'Ecuador', 'Ecuador'), 
('EG', 'Egypt', 'مصر'), 
('SV', 'El Salvador', 'El Salvador'), 
('GQ', 'Equatorial Guinea', 'Guinea Ecuatorial'), 
('ER', 'Eritrea', 'Ertra'), 
('EE', 'Estonia', 'Eesti'), 
('ET', 'Ethiopia', 'Ityop&#39;iya'), 
('FK', 'Falkland Islands', 'Falkland Islands'), 
('FO', 'Faroe Islands', 'Faroe Islands'), 
('FJ', 'Fiji', 'Fiji'), 
('FI', 'Finland', 'Suomi'), 
('FR', 'France', 'France'), 
('GF', 'French Guiana', 'French Guiana'), 
('PF', 'French Polynesia', 'French Polynesia'), 
('TF', 'French Southern Territories', 'French Southern Territories'), 
('GA', 'Gabon', 'Gabon'), 
('GM', 'Gambia', 'Gambia'), 
('GE', 'Georgia', 'საქართველო'), 
('DE', 'Germany', 'Deutschland'), 
('GH', 'Ghana', 'Ghana'), 
('GI', 'Gibraltar', 'Gibraltar'), 
('GR', 'Greece', 'Ελλάς'), 
('GL', 'Greenland', 'Greenland'), 
('GD', 'Grenada', 'Grenada'), 
('GP', 'Guadeloupe', 'Guadeloupe'), 
('GU', 'Guam', 'Guam'), 
('GT', 'Guatemala', 'Guatemala'), 
('GG', 'Guernsey', 'Guernsey'), 
('GN', 'Guinea', 'Guinée'), 
('GW', 'Guinea-Bissau', 'Guiné-Bissau'), 
('GY', 'Guyana', 'Guyana'), 
('HT', 'Haiti', 'Haïti'), 
('HM', 'Heard Island and McDonald Island', 'Heard Island and McDonald Island'), 
('HN', 'Honduras', 'Honduras'), 
('HK', 'Hong Kong', 'Hong Kong'), 
('HU', 'Hungary', 'Magyarország'), 
('IS', 'Iceland', 'Ísland'), 
('IN', 'India', 'India'), 
('ID', 'Indonesia', 'Indonesia'), 
('IR', 'Iran', 'ایران'), 
('IQ', 'Iraq', 'العراق'), 
('IE', 'Ireland', 'Ireland'), 
('IM', 'Isle of Man', 'Isle of Man'), 
('IL', 'Israel', 'ישראל'), 
('IT', 'Italy', 'Italia'), 
('JM', 'Jamaica', 'Jamaica'), 
('JP', 'Japan', '日本'), 
('JE', 'Jersey', 'Jersey'), 
('JO', 'Jordan', 'الاردن'), 
('KZ', 'Kazakhstan', 'Қазақстан'), 
('KE', 'Kenya', 'Kenya'), 
('KI', 'Kiribati', 'Kiribati'), 
('KW', 'Kuwait', 'الكويت'), 
('KG', 'Kyrgyzstan', 'Кыргызстан'), 
('LA', 'Laos', 'ນລາວ'), 
('LV', 'Latvia', 'Latvija'), 
('LB', 'Lebanon', 'لبنان'), 
('LS', 'Lesotho', 'Lesotho'), 
('LR', 'Liberia', 'Liberia'), 
('LY', 'Libya', 'ليبيا'), 
('LI', 'Liechtenstein', 'Liechtenstein'), 
('LT', 'Lithuania', 'Lietuva'), 
('LU', 'Luxembourg', 'Lëtzebuerg'), 
('MO', 'Macao', 'Macao'), 
('MK', 'Macedonia', 'Македонија'), 
('MG', 'Madagascar', 'Madagasikara'), 
('MW', 'Malawi', 'Malawi'), 
('MY', 'Malaysia', 'Malaysia'), 
('MV', 'Maldives', 'ގުޖޭއްރާ ޔާއްރިހޫމްޖ'), 
('ML', 'Mali', 'Mali'), 
('MT', 'Malta', 'Malta'), 
('MH', 'Marshall Islands', 'Marshall Islands'), 
('MQ', 'Martinique', 'Martinique'), 
('MR', 'Mauritania', 'موريتانيا'), 
('MU', 'Mauritius', 'Mauritius'), 
('YT', 'Mayotte', 'Mayotte'), 
('MX', 'Mexico', 'México'), 
('FM', 'Micronesia', 'Micronesia'), 
('MD', 'Moldova', 'Moldova'), 
('MC', 'Monaco', 'Monaco'), 
('MN', 'Mongolia', 'Монгол Улс'), 
('ME', 'Montenegro', 'Црна Гора'), 
('MS', 'Montserrat', 'Montserrat'), 
('MA', 'Morocco', 'المغرب'), 
('MZ', 'Mozambique', 'Moçambique'), 
('MM', 'Myanmar', 'Burma'), 
('NA', 'Namibia', 'Namibia'), 
('NR', 'Nauru', 'Naoero'), 
('NP', 'Nepal', 'नेपाल'), 
('NL', 'Netherlands', 'Nederland'), 
('AN', 'Netherlands Antilles', 'Netherlands Antilles'), 
('NC', 'New Caledonia', 'New Caledonia'), 
('NZ', 'New Zealand', 'New Zealand'), 
('NI', 'Nicaragua', 'Nicaragua'), 
('NE', 'Niger', 'Niger'), 
('NG', 'Nigeria', 'Nigeria'), 
('NU', 'Niue', 'Niue'), 
('NF', 'Norfolk Island', 'Norfolk Island'), 
('MP', 'Northern Mariana Islands', 'Northern Mariana Islands'), 
('KP', 'North Korea', '조선'), 
('NO', 'Norway', 'Norge'), 
('OM', 'Oman', 'عمان'), 
('PK', 'Pakistan', 'پاکستان'), 
('PW', 'Palau', 'Belau'), 
('PS', 'Palestinian Territories', 'Palestinian Territories'), 
('PA', 'Panama', 'Panamá'), 
('PG', 'Papua New Guinea', 'Papua New Guinea'), 
('PY', 'Paraguay', 'Paraguay'), 
('PE', 'Peru', 'Perú'), 
('PH', 'Philippines', 'Pilipinas'), 
('PN', 'Pitcairn', 'Pitcairn'), 
('PL', 'Poland', 'Polska'), 
('PT', 'Portugal', 'Portugal'), 
('PR', 'Puerto Rico', 'Puerto Rico'), 
('QA', 'Qatar', 'قطر'), 
('RE', 'Reunion', 'Reunion'), 
('RO', 'Romania', 'România'), 
('RU', 'Russia', 'Россия'), 
('RW', 'Rwanda', 'Rwanda'), 
('SH', 'Saint Helena', 'Saint Helena'), 
('KN', 'Saint Kitts and Nevis', 'Saint Kitts and Nevis'), 
('LC', 'Saint Lucia', 'Saint Lucia'), 
('PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon'), 
('VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines'), 
('WS', 'Samoa', 'Samoa'), 
('SM', 'San Marino', 'San Marino'), 
('ST', 'São Tomé and Príncipe', 'São Tomé and Príncipe'), 
('SA', 'Saudi Arabia', 'المملكة العربية السعودية'), 
('SN', 'Senegal', 'Sénégal'), 
('RS', 'Serbia', 'Србија'), 
('CS', 'Serbia and Montenegro', 'Србија и Црна Гора'), 
('SC', 'Seychelles', 'Seychelles'), 
('SL', 'Sierra Leone', 'Sierra Leone'), 
('SG', 'Singapore', 'Singapura'), 
('SK', 'Slovakia', 'Slovensko'), 
('SI', 'Slovenia', 'Slovenija'), 
('SB', 'Solomon Islands', 'Solomon Islands'), 
('SO', 'Somalia', 'Soomaaliya'), 
('ZA', 'South Africa', 'South Africa'), 
('GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands'), 
('KR', 'South Korea', '한국'), 
('ES', 'Spain', 'España'), 
('LK', 'Sri Lanka', 'Sri Lanka'), 
('SD', 'Sudan', 'السودان'), 
('SR', 'Suriname', 'Suriname'), 
('SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen'), 
('SZ', 'Swaziland', 'Swaziland'), 
('SE', 'Sweden', 'Sverige'), 
('CH', 'Switzerland', 'Schweiz'), 
('SY', 'Syria', 'سوريا'), 
('TW', 'Taiwan', '台灣'), 
('TJ', 'Tajikistan', 'Тоҷикистон'), 
('TZ', 'Tanzania', 'Tanzania'), 
('TH', 'Thailand', 'ราชอาณาจักรไทย'), 
('TL', 'Timor-Leste', 'Timor-Leste'), 
('TG', 'Togo', 'Togo'), 
('TK', 'Tokelau', 'Tokelau'), 
('TO', 'Tonga', 'Tonga'), 
('TT', 'Trinidad and Tobago', 'Trinidad and Tobago'), 
('TN', 'Tunisia', 'تونس'), 
('TR', 'Turkey', 'Türkiye'), 
('TM', 'Turkmenistan', 'Türkmenistan'), 
('TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands'), 
('TV', 'Tuvalu', 'Tuvalu'), 
('UG', 'Uganda', 'Uganda'), 
('UA', 'Ukraine', 'Україна'), 
('AE', 'United Arab Emirates', 'الإمارات العربيّة المتّحدة'), 
('GB', 'United Kingdom', 'United Kingdom'), 
('US', 'United States', 'United States'), 
('UM', 'United States minor outlying islands', 'United States minor outlying islands'), 
('UY', 'Uruguay', 'Uruguay'), 
('UZ', 'Uzbekistan', 'O&#39;zbekiston'), 
('VU', 'Vanuatu', 'Vanuatu'), 
('VA', 'Vatican City', 'Città del Vaticano'), 
('VE', 'Venezuela', 'Venezuela'), 
('VN', 'Vietnam', 'Việt Nam'), 
('VG', 'Virgin Islands, British', 'Virgin Islands, British'), 
('VI', 'Virgin Islands, U.S.', 'Virgin Islands, U.S.'), 
('WF', 'Wallis and Futuna', 'Wallis and Futuna'), 
('EH', 'Western Sahara', 'الصحراء الغربية'), 
('YE', 'Yemen', 'اليمن'), 
('ZM', 'Zambia', 'Zambia'), 
('ZW', 'Zimbabwe', 'Zimbabwe');
